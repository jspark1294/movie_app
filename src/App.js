import React from 'react';
import Home from "./routes/Home";
import './App.css';
import {HashRouter, Route,BrowserRouter} from "react-router-dom";
import About from './routes/About';
import Navigation from "./components/Navigation";
import Detail from "./components/Detail";

function App() {
    return(
        <BrowserRouter>
            <Navigation/>
            <Route exact path="/"  component={Home}/>
            <Route path="/about" component={About}/>
            <Route exact path="/movie-detail/:id" component={Detail}/>
        </BrowserRouter>
    )
}
export default App;
